from joblib import Parallel, delayed, load, dump
import h5py
import pathlib
import numpy as np
import natsort
from hmm import hmm
import multiprocessing

A30_IDs = [
           1348,#
           #2103,#
           ]

C30_IDs = [
           1022,#
           ]

A20C20_IDs = [
              1716,#
              #1720,#
              ]

AC20_IDs =[
           1028,#
          ]


def phase_slice(input_data, pixel_ID, phase_ID=None):
    signal = input_data['raw_signal'][pixel_ID]
    if phase_ID is None:
        return signal
    start_idx = int(input_data['t_start'][phase_ID])
    stop_idx = int(input_data['t_stop'][phase_ID])
    return signal[start_idx:stop_idx]

def run(input_data, file_ID, pixel_ID):
    Ntrials = 1000
    Nstates = 20
    gamma = .1
    alpha = 10
    ell = .1
    downsample = 1 # 1 means no downsample

    try:
        data1 = phase_slice(input_data, pixel_ID, 1)[::downsample]
        data3 = phase_slice(input_data, pixel_ID, 3)[::downsample]
        data5 = phase_slice(input_data, pixel_ID, 5)[:-100000:downsample] # ignore the last part of the trace
    except:
        print('iHMM_{}_{} FAILED'.format(file_ID, pixel_ID))
        return

    dhmm1 = hmm(data1,
                    Nstates=Nstates,
                    gamma=gamma,
                    alpha=alpha,
                    ell=ell,
                    iHMM=True,
                    Ngrid=100,
                    prefix='Outfiles/iHMM_{}_{}_control'.format(file_ID, pixel_ID))
    dhmm1.run_Gibbs(Ntrials=Ntrials, plot_status=False, save=True)

    # This block is commented out because it will already be covered by calibrate-addastate-go
    # dhmm3 = hmm(data1,
    #                 Nstates=Nstates,
    #                 gamma=gamma,
    #                 alpha=alpha,
    #                 ell=ell,
    #                 iHMM=True,
    #                 prefix='Outfiles/iHMM_{}_{}_missmatch'.format(file_ID, pixel_ID))
    # dhmm3.run_Gibbs(Ntrials=Ntrials, plot_status=False, save=True)

    dhmm5 = hmm(data1,
                    Nstates=Nstates,
                    gamma=gamma,
                    alpha=alpha,
                    ell=ell,
                    iHMM=True,
                    prefix='Outfiles/iHMM_{}_{}_match'.format(file_ID, pixel_ID))
    dhmm5.run_Gibbs(Ntrials=Ntrials, plot_status=False, save=True)

input_file_path = pathlib.Path('/data/data/H5_files')
num_cores = multiprocessing.cpu_count()

def process(file_IDs):
    for file_ID in file_IDs:    
        with h5py.File(input_file_path / f'G3A-{file_ID}.h5', mode='r') as h5:
            pixels = list(h5['pixel_info'].keys())
            num_pixels = len(pixels)
            sig_len = len(h5['pixel_info'][pixels[0]]['raw_signal'])
            phase_len = len(h5['exp_info']['phase_info'])
            data = {'raw_signal': np.empty(shape=(len(pixels), sig_len)),
                    't_start': np.empty(phase_len), 
                    't_stop': np.empty(phase_len)
                   }
            for ix, pixel in enumerate(pixels):
                pixel_data = h5['pixel_info'][pixel]['raw_signal'][()]
                data['raw_signal'][ix, :] = pixel_data
            for ix, phase in enumerate(natsort.natsorted(list(h5['exp_info']['phase_info'].keys()))):
                data['t_start'][ix] = h5['exp_info']['phase_info'][phase]['t_start'][()]
                data['t_stop'][ix] = h5['exp_info']['phase_info'][phase]['t_stop'][()]

        shm_name = f'/dev/shm/{file_ID}-input-joblib.data'
        memmap_location = pathlib.Path(shm_name)
        dump(data, memmap_location)
        data = load(memmap_location, mmap_mode='r')
        Parallel(n_jobs=num_cores)(delayed(run)(data, file_ID, pixel) for pixel in range(num_pixels))
        memmap_location.unlink()

if __name__ == "__main__":
    process(["1716"])
