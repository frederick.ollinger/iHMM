import time
import copy
import numpy as np
import scipy.sparse as sparse
import matplotlib.pyplot as plt
import scipy.stats as stats
from scipy.special import gammaln
#import cupy
import numba as nb
#from libCore.util.lib_profile import profile_runtime

#numpy forward filter
#@profile_runtime
def forward_filter_np(pi0, pis, lhood, Nstates, Nbins):
    forward = np.zeros((Nstates, Nbins))
    forward[:, 0] = lhood[:, 0] * pi0[0, :]
    forward[:, 0] /= np.sum(forward[:, 0])
    for n in range(1, Nbins):
        forward[:, n] = lhood[:, n] * (pis.T @ forward[:, n - 1])
        forward[:, n] /= np.sum(forward[:, n])
    return forward

#numba forward filter
#@profile_runtime
@nb.jit(nopython=True)
def forward_filter_nb(pi0, pis, lhood, Nstates, Nbins):
    forward = np.zeros((Nstates, Nbins))
    forward[:, 0] = lhood[:, 0] * pi0[0, :]
    forward[:, 0] /= np.sum(forward[:, 0])
    for n in range(1, Nbins):
        forward[:, n] = lhood[:, n] * (pis.T @ forward[:, n - 1])
        forward[:, n] /= np.sum(forward[:, n])
    return forward

#cupy forward filter
#@profile_runtime
def forward_filter_cupy(pi0,pis,lhood,Nstates,Nbins):
    pi0 = cupy.array(pi0)
    pis = cupy.array(pis)
    lhood = cupy.array(lhood)
    forward = cupy.zeros((Nstates, Nbins))
    forward[:, 0] = lhood[:, 0] * pi0[0, :]
    forward[:, 0] /= np.sum(forward[:, 0])
    for n in range(1, Nbins):
        forward[:, n] = cupy.multiply(lhood[:, n], (cupy.dot(pis.T, forward[:, n - 1])))
        forward[:, n] /= cupy.sum(forward[:, n])
    return forward

#numpy backward filter
#@profile_runtime
def backward_filter_np(forward,pis,Nstates,Nbins,outcomes,states):
    states[0, -1] = np.random.choice(Nstates, p=forward[:, -1])
    for n in np.arange(Nbins - 1, 0, -1) - 1:
        backward = forward[:, n] * pis[:, states[0, n + 1]]
        backward /= np.sum(backward)
        states[0, n] = np.random.choice(Nstates, p=backward)
    return backward, states

#numba backward filter
#@profile_runtime
@nb.jit(nopython=True)
def backward_filter_nb(forward,pis,Nstates,Nbins,outcomes,states):
    x = outcomes[np.searchsorted(np.cumsum(forward[:,-1]), np.random.rand(1))]
    states[0,-1] = x[0]
    for nid in range(1,Nbins):
        n = Nbins - nid - 1
        backward = forward[:, n] * pis[:, states[0, n + 1]]
        backward /= np.sum(backward)
        x = outcomes[np.searchsorted(np.cumsum(backward), np.random.rand(1))]
        states[0, n] = x[0]
    return backward, states

#numba sample drift
#@profile_runtime
@nb.jit(nopython=False)
def sample_drift_nb(data,phis,states,Nstates,Nbins,Ngrid,Kstar,KgridInv):

    Taus = np.zeros((1,Nbins))
    for k in range(Nstates):
        idx = (states == k)
        Taus[idx] = phis[k,1]
        data[idx] -= phis[k,0]
    Taus = sparse.dia_matrix((Taus,0), shape=(Nbins,Nbins))
    Ktilde = np.linalg.inv(KgridInv + KgridInv @ Kstar.T @ Taus @ Kstar @ KgridInv)
    utilde = Ktilde @ KgridInv @ Kstar.T @ Taus @ data.T
    Kchol = np.linalg.cholesky(Ktilde)
    drift = (Kstar @ KgridInv @ (utilde + Kchol.T @ np.random.randn(Ngrid, 1))).T
    return drift

class hmm:

    class variables:

        def __init__(self, data, Nstates=20, alpha=10, gamma=.1, Ngrid=500, ell=.1, iHMM=True,use_numba=True,use_GPU=False):

            lock_in = np.zeros((1, Nstates))
            beta = np.ones((1, Nstates)) / Nstates
            pi0 = np.ones((1, Nstates)) / Nstates
            pis = np.ones((Nstates, Nstates)) / Nstates
            phis = np.zeros((Nstates, 2))
            phis[:, 1] = .1

            self.data = []
            self.iHMM = iHMM
            self.lock_in = lock_in
            self.Nbins = []
            self.Ngrid = Ngrid
            self.Nstates = Nstates
            self.states = []
            self.pi0 = pi0
            self.pis = pis
            self.phis = phis
            self.ulist = []
            self.drift = []
            self.ell = ell
            self.Kstar = []
            self.KgridInv = []
            self.use_numba = use_numba
            self.use_GPU = use_GPU
            if iHMM:
                self.gamma = gamma  # concentration on beta
                self.alpha = alpha  # concentration on pis
                self.beta = beta

        def init(self, data, prior=None, setup_ell=None, sample_beta=False):

            if prior is None:
                prior = self.copy()
            if setup_ell is None:
                setup_ell = self.ell

            Nbins = data.shape[1]
            Ngrid = self.Ngrid
            Nstates = self.Nstates
            iHMM = self.iHMM
            lock_in = self.lock_in
            pi0 = self.pi0
            pis = self.pis
            phis = self.phis
            ell = self.ell

            if iHMM:
                gamma = self.gamma
                alpha = self.alpha
                beta = self.beta

                if sample_beta:
                    beta[0,:] = np.random.dirichlet(gamma * np.ones(Nstates)).reshape((1, Nstates))

                pi0[0,:] = np.random.gamma(alpha*beta[0,:])
                pi0[0,:] /= np.sum(pi0)

                for k in range(Nstates):
                    pis[k,:] = np.random.gamma(alpha*beta)
                    pis[k,k] = 10
                    pis[k,:] /= np.sum(pis[k,:])
            else:
                pi0[0,:] = np.random.gamma(prior.pi0[0,:])
                pi0[0,:] /= np.sum(pi0)
                idx = lock_in[0,:] == 1
                pi0[0, idx] = prior.pi0[0,idx]*np.sum(pi0[0,idx])/np.sum(prior.pi0[0,idx])

                for k in range(Nstates):
                    pis[k,:] = np.random.gamma(prior.pis[k,:])
                    pis[k,:] /= np.sum(pis[k,:])
                    idx = lock_in[0,:] == 1
                    pis[0, idx] = prior.pis[0,idx]*np.sum(pis[0,idx])/np.sum(prior.pis[0,idx])

                if (np.sum(pi0[0,:])-1 > 1e-10) or (np.sum(pis[0,:])-1 > 1e-10):
                    raise ValueError('Something did not add up here in sample_pis().')

            for k in range(Nstates):
                if not lock_in[0,k]:
                    phis[k, 0] = prior.phis[k, 0] + 10*np.random.randn()
                    phis[k, 1] = np.random.gamma(2, prior.phis[k,1]/2)
                    if k == 0:
                        phis[k,0] = 0

            states = np.zeros((1,Nbins))

            ulist = np.random.rand(1, Nbins)

            # Start with a bad sample for drift
            setup_Ngrid = Ngrid
            Kstar, KgridInv = self.setup_GP_matrices(Nbins=Nbins, Ngrid=setup_Ngrid, ell=setup_ell)
            Taus = sparse.dia_matrix((np.ones(Nbins), 0), shape=(Nbins, Nbins))
            Ktilde = np.linalg.inv(KgridInv
                                   + KgridInv @ Kstar.T @ Taus @ Kstar @ KgridInv
                                   + .1 * np.eye(setup_Ngrid))
            utilde = Ktilde @ KgridInv @ Kstar.T @ Taus @ data.T
            drift = (Kstar @ KgridInv @ utilde).T

            # Now set up matrices for real
            Kstar, KgridInv = self.setup_GP_matrices(Nbins=Nbins, Ngrid=Ngrid, ell=ell)

            self.data = data
            self.iHMM = iHMM
            self.lock_in = lock_in
            self.Nbins = Nbins
            self.Ngrid = Ngrid
            self.Nstates = Nstates
            self.states = states
            self.pi0 = pi0
            self.pis = pis
            self.phis = phis
            self.ulist = ulist
            self.drift = drift
            self.ell = ell
            self.Kstar = Kstar
            self.KgridInv = KgridInv

        def copy(self):

            new_variables = copy.deepcopy(self)

            return new_variables

        def setup_GP_matrices(self, Nbins=None, Ngrid=500, sig=1, ell=.1):

            if Nbins is None: Nbins = self.Nbins
            xgrid = np.linspace(0, Nbins - 1, Ngrid).reshape((1, Ngrid))
            xdata = np.arange(Nbins).reshape((1, Nbins))
            ell = ell * Nbins

            Kstar = sig ** 2 * np.exp(-.5 * (xgrid - xdata.T) ** 2 / ell ** 2)
            Kgrid = sig ** 2 * np.exp(-.5 * (xgrid - xgrid.T) ** 2 / ell ** 2)
            KgridInv = np.linalg.inv(Kgrid + .01 * np.eye(Ngrid))

            return Kstar, KgridInv

        def thin_states(self, filename=None, Nburn=-1):

            iHMM = self.iHMM

            if filename is not None:
                Ntrials, Nstates = np.genfromtxt(filename + '_phis.csv', delimiter=',').shape
                Nstates = int(Nstates / 2)
                if Nburn is -1:
                    Nburn = int(round(Ntrials / 2))

                phis = np.mean(np.genfromtxt(filename + '_phis.csv', delimiter=',')[Nburn:, :],
                               0).reshape(Nstates, 2)
                pis = np.mean(np.genfromtxt(filename + '_pis.csv', delimiter=',')[Nburn:, :],
                              0).reshape(Nstates, Nstates)
                pi0 = np.ones((1,Nstates))

                states = np.genfromtxt(filename + '_states.csv', delimiter=',')[Nburn:, :]
                states = stats.mode(states, axis=0).mode
                state_IDs = np.unique(states).astype(int)
            else:
                pi0 = self.pi0.copy()
                pis = self.pis.copy()
                phis = self.phis.copy()
                states = self.states.copy()
                state_IDs = np.unique(states).astype(int)

            for i, k in enumerate(state_IDs):
                idx = states==k
                states[idx] = i

            Nstates = len(state_IDs)

            phis = phis[state_IDs, :]

            pi0 = pi0[0, state_IDs]
            pi0 /= np.sum(pi0)
            pi0 = pi0.reshape((1,-1))

            pis = pis[state_IDs, :]
            pis = pis[:, state_IDs]
            pis[pis < 1e-10] = 0
            for k in range(len(state_IDs)):
                pis[k,:] /= np.sum(pis[k,:])

            # Label switching
            mode = int(stats.mode(states[0,:]).mode[0])
            phis[:,0] -= phis[mode, 0]
            pi0[0,0], pi0[0,mode] = pi0[0,mode], pi0[0,0]
            pis[0,:], pis[mode,:] = pis[mode,:], pis[0,:]
            pis[:,0], pis[:,mode] = pis[:,mode], pis[:,0]
            phis[0,:], phis[mode,:] = phis[mode,:], phis[0,:]
            states[states==0] = -1
            states[states==mode] = 0
            states[states==-1] = mode

            lock_in = np.ones((1, Nstates))

            self.iHMM = False
            self.lock_in = lock_in
            self.Nstates = Nstates
            self.pi0 = pi0
            self.pis = pis
            self.phis = phis
            self.states = states
            if iHMM:
                del self.gamma
                del self.alpha
                del self.beta

        def add_state(self, Nadd=1, link_to=None):

            Nstates_old = self.Nstates
            Nstates_new = Nstates_old + Nadd

            lock_in_old = self.lock_in
            lock_in_new = np.zeros((1,Nstates_new))
            lock_in_new[0,:-Nadd] = lock_in_old[0,:]

            if link_to is None:
                link_to = lock_in_new.copy()
            elif link_to is 0:
                link_to = np.zeros((1,Nstates_new))
                link_to[0,0] = 1
            link_to = link_to.reshape((1,-1))

            pi0_old = self.pi0
            pi0_new = np.ones((1, Nstates_new))/1000
            pi0_new[0,:-Nadd] = pi0_old[0,:]
            pi0_new /= np.sum(pi0_new)

            pis_old = self.pis
            pis_new = np.ones((Nstates_new, Nstates_new))/Nstates_new
            pis_new[:-Nadd, :-Nadd] = pis_old[:,:]
            for i in range(Nadd):
                pis_new[:, -(i+1)] = link_to[0,:]/1000
                pis_new[-(i+1), :] = link_to[0,:]/1000
                pis_new[-(i+1), -(i+1)] = 10
            for k in range(Nstates_new):
                pis_new[k,:] /= np.sum(pis_new[k,:])

            phis_old = self.phis
            phis_new = np.zeros((Nstates_new, 2))
            phis_new[:-Nadd, :] = phis_old[:,:]
            for i in range(Nadd):
                phis_new[-(i+1),0] = 20
                phis_new[-(i+1),1] = .1
            phis_new[:,0] -= phis_new[0,0]

            self.pi0 = pi0_new
            self.pis = pis_new
            self.phis = phis_new
            self.Nstates = Nstates_new
            self.lock_in = lock_in_new

    class history:

        def __init__(self, vals, save_name):

            self.save_name = save_name

            open(save_name+'_pis.csv', 'w').close()
            open(save_name+'_phis.csv', 'w').close()
            open(save_name+'_states.csv', 'w').close()

        def checkpoint(self, vals):

            save_name = self.save_name

            temp = vals.pis[:,:].reshape((1,-1))
            with open(save_name+'_pis.csv', 'a') as file:
                file.write(', '.join(str(x) for x in temp[0,:])+'\n')

            temp = vals.phis[:,:].reshape((1,-1))
            with open(save_name+'_phis.csv', 'a') as file:
                file.write(', '.join(str(x) for x in temp[0,:])+'\n')

            temp = vals.states[:,:].reshape((1,-1))
            with open(save_name+'_states.csv', 'a') as file:
                file.write(', '.join(str(x) for x in temp[0,:])+'\n')

    def __init__(self, data, Nstates=20, alpha=10, gamma=.1,
                 Ngrid=500, ell=.5, iHMM=True, prefix='test'):

        data = np.reshape(data, (1,-1))
        Nbins = data.shape[1]

        self.data = data
        self.Nbins = Nbins
        self.prefix = prefix
        self.prior = hmm.variables(data, Nstates=Nstates, alpha=alpha, gamma=gamma,
                                        Ngrid=Ngrid, ell=ell, iHMM=iHMM)
        self.old = None
        self.MAP = None
        self.Ntrials = None

    def loglikelihood(self, vals=None, phis=None, states=None, drift=None, ulist=None, sum_it=True):

        if vals is None: vals = self.old
        if phis is None: phis = vals.phis.copy()
        if states is None: states = vals.states.copy()
        if drift is None: drift = vals.drift.copy()
        if ulist is None: ulist = vals.ulist.copy()

        data = self.data + ulist - drift
        Nbins = vals.Nbins
        Nstates = vals.Nstates
        loglhood = np.zeros((1, Nbins))
        for k in range(Nstates):
            idx = states==k
            mu = phis[k,0]
            tau = phis[k,1]
            loglhood[idx] = -.5*np.log(2*np.pi/tau) - .5*tau*((data[idx]-mu))**2

        if sum_it: loglhood = np.sum(loglhood)

        return loglhood

    def sample_states(self):

        old = self.old
        Nbins = old.Nbins
        Nstates = old.Nstates

        # Make log likelihood matrix
        loglhood = np.zeros((Nstates, Nbins)) # likelihoood of the states
        for k in range(Nstates):
            loglhood[k,:] = self.loglikelihood(states=k*np.ones((1,Nbins)), sum_it=False)

        # Softmax log likelihood to get likelihood matrix
        lhood = np.exp(loglhood-np.max(loglhood,0))
        lhood /= np.sum(lhood,0) + 1e-100

        # Forward filter
        pi0 = old.pi0
        pis = old.pis
#        forward1 = forward_filter_np(pi0,pis,lhood,Nstates,Nbins)
        forward2 = forward_filter_nb(pi0,pis,lhood,Nstates,Nbins)
#        forward3 = forward_filter_cupy(pi0,pis,lhood,Nstates,Nbins)
        forward = forward2

        # Backwards sample
        outcomes = np.arange(0, Nstates, 1,dtype=int)
        states = np.zeros((1, Nbins), dtype=int)

        #numpy backward filter
#        backward1, states1 = backward_filter_np(forward, pis, int(Nstates), int(Nbins),outcomes,states)
        backward2, states2 = backward_filter_nb(forward, pis, int(Nstates), int(Nbins), outcomes, states)
#        backward = backward2
        states = states2

        if np.isnan(states).any():
            raise ValueError('Nan encountered in sample_states().')

        return states

    def sample_drift_cupy(self):

        old = self.old
        data = self.data + old.ulist
        phis = old.phis
        states = old.states
        Nstates = old.Nstates
        Nbins = old.Nbins
        Ngrid = old.Ngrid
        Kstar = old.Kstar
        KgridInv = old.KgridInv

        #First loop
        Taus = np.zeros((1,Nbins))
        for k in range(Nstates):
            idx = states == k
            Taus[idx] = phis[k,1]
            data[idx] -= phis[k,0]

        #cupy load
        Taus = cupy.sparse.dia_matrix(arg1=(Taus.astype(float),0), shape=(Nbins,Nbins))
        KgridInv = cupy.array(KgridInv.astype(float))
        Kstar = cupy.array(Kstar.astype(float))
        data = cupy.array(data.astype(float))
        rands = cupy.array(np.random.randn(Ngrid, 1))

        # cupy inv
        Ktilde = cupy.linalg.inv(KgridInv + ((KgridInv @ Kstar.T) @ (Taus * (Kstar @ KgridInv))))

        #cupy  utilde
        utilde = ((Ktilde @ KgridInv) @ Kstar.T) @ (Taus * data.T)

        #cupy choel
        Kchol = cupy.linalg.cholesky(Ktilde)

        #cupy drift computation
        drift = (Kstar @ KgridInv @ (utilde + Kchol.T @ rands)).T

        #numpy drift
        drift = cupy.asnumpy(drift)

        #check
        if np.isnan(drift).any():
            raise ValueError('Nan encountered in sample_drift().')

        return drift

    #@profile_runtime
    def sample_drift_np(self):

        old = self.old
        data = self.data + old.ulist
        phis = old.phis
        states = old.states
        Nstates = old.Nstates
        Nbins = old.Nbins
        Ngrid = old.Ngrid
        Kstar = old.Kstar
        KgridInv = old.KgridInv

        Taus = np.zeros((1, Nbins))
        for k in range(Nstates):
            idx = states == k
            Taus[idx] = phis[k, 1]
            data[idx] -= phis[k, 0]
        Taus = sparse.dia_matrix((Taus, 0), shape=(Nbins, Nbins))

        Ktilde = np.linalg.inv(KgridInv + KgridInv @ Kstar.T @ Taus @ Kstar @ KgridInv)
        utilde = Ktilde @ KgridInv @ Kstar.T @ Taus @ data.T

        try:
            Kchol = np.linalg.cholesky(Ktilde)
        except:
            U, S, _ = np.linalg.svd(Ktilde)
            _, Kchol = np.linalg.qr(np.sqrt(np.diag(S)) @ U.T)

        drift = (Kstar @ KgridInv @ (utilde + Kchol.T @ np.random.randn(Ngrid, 1))).T

        if np.isnan(drift).any():
            raise ValueError('Nan encountered in sample_drift().')

        return drift

    def sample_pis(self, vals, prior):

        Nbins = vals.Nbins
        Nstates = vals.Nstates
        states = vals.states
        lock_in = vals.lock_in

        if np.sum(lock_in) > Nstates/2:
            alpha = Nbins/Nstates
        else:
            alpha = 1

        pi0_old = vals.pi0
        pi0_new = np.zeros((1, Nstates))
        pis_old = vals.pis
        pis_new = np.zeros((Nstates, Nstates))

        if Nstates==1:
            pi0_new = np.ones((1,1))
            pis_new = np.ones((1,1))
            return pi0_new, pis_new

        # Sample pi0
        counts = np.zeros((1, Nstates))
        counts[0,states[0,0]] = 1
        pi0_new[0,:] = np.random.gamma(counts[0,:] + alpha*prior.pi0[0,:])
        pi0_new[0,:] /= np.sum(pi0_new)
        # Now to treat the lock_in cases
        # idx = lock_in[0,:] == 1
        # pi0_new[0, idx] = pi0_old[0,idx]*np.sum(pi0_new[0,idx])/np.sum(pi0_old[0,idx])

        # Count transitions
        counts = np.zeros((Nstates, Nstates))
        for n in range(1,Nbins):
            counts[states[0,n-1], states[0,n]] += 1

        # Sample new transition probabilities from Dirichlet
        for k in range(Nstates):
            pis_new[k,:] = np.random.gamma(counts[k,:]+alpha*prior.pis[k,:])
            pis_new[k,:] /= np.sum(pis_new[k,:])
            # Now to treat the lock_in cases
            # idx = lock_in[0,:] == 1
            # pis_new[k, idx] = pis_old[k,idx]*np.sum(pis_new[k,idx])/np.sum(pis_old[k,idx])

        if (np.isnan(pi0_new).any()) or (np.isnan(pis_new).any()):
            raise ValueError('Nan encountered in sample_pis().')

        if (np.sum(pi0_new[0, :]) - 1 > 1e-10) or (np.sum(pis_new[0, :]) - 1 > 1e-10):
            raise ValueError('Something did not add up here in sample_pis().')

        return pi0_new, pis_new

    def sample_pis_iHMM(self, vals):

        Nbins = vals.Nbins
        Nstates = vals.Nstates
        states = vals.states
        alpha = vals.alpha
        gamma = vals.gamma
        eta = np.ones((1,Nstates))/Nstates
        prop_width = 10000
        counts = np.zeros((Nstates+1, Nstates))
        pis = np.zeros((Nstates+1, Nstates))

        # Count transitions
        counts[0,states[0,0]] = 1
        for n in range(1,Nbins):
            counts[states[0,n-1]+1, states[0,n]] += 1

        # Sample beta using Metropolis-Hastings
        beta_old = vals.beta.copy()
        prob_old = np.sum(gammaln(counts+alpha*beta_old))\
                    + np.sum((gamma/Nstates-1)*np.log(beta_old)\
                             - (Nstates+1)*gammaln(alpha*beta_old))

        beta_new = np.random.dirichlet(prop_width*beta_old[0,:]+1).reshape((1,-1))
        prob_new = np.sum(gammaln(counts+alpha*beta_new))\
                    + np.sum((gamma/Nstates-1)*np.log(beta_new)\
                             - (Nstates+1)*gammaln(alpha*beta_new))

        acc_prob = prob_new - prob_old\
                 + stats.dirichlet.logpdf(beta_old[0,:], prop_width * beta_new[0,:] + 1)\
                 - stats.dirichlet.logpdf(beta_new[0,:], prop_width * beta_old[0,:] + 1)
        if acc_prob > np.log(np.random.rand()):
            print('iHMM accepted')
        else:
            beta_new = beta_old
            print('iHMM rejected')

        # Sample new transition probabilities from Dirichlet
        for k in range(Nstates+1):
            pis[k, :] = np.random.gamma(counts[k, :] + alpha*beta_new[0, :])
            pis[k, :] /= np.sum(pis[k, :])

        pi0_new = np.zeros((1,Nstates))
        pis_new = np.zeros((Nstates,Nstates))
        pi0_new[0,:] = pis[0,:]
        pis_new[:,:] = pis[1:,:]

        if (np.isnan(beta_new).any()) or (np.isnan(pi0_new).any()) or (np.isnan(pis_new).any()):
            raise ValueError('Nan encountered in sample_pis_iHMM().')

        if (np.sum(pi0_new[0, :]) - 1 > 1e-10) or (np.sum(pis_new[0, :]) - 1 > 1e-10):
            raise ValueError('Something did not add up here in sample_pis().')

        return beta_new, pi0_new, pis_new

    def sample_phis(self, vals, prior):

        data = self.data + vals.ulist - vals.drift

        Nbins = vals.Nbins
        Nstates = vals.Nstates
        states = vals.states
        lock_in = vals.lock_in
        phis_old = vals.phis
        phis_new = phis_old.copy()

        # Sample parameters
        for k in range(Nstates):
            if not lock_in[0,k]:
                idx = states == k
                Nk = np.sum(idx)
                mu = phis_old[k,0]
                tau = phis_old[k,1]
                # Sample mean
                prior_mu = prior.phis[k,0]
                prior_tau = .01
                post_tau = prior_tau + Nk*tau
                post_mu = (prior_mu*prior_tau + np.sum(data[idx])*tau)/post_tau
                mu = post_mu + np.random.randn()/np.sqrt(post_tau)
                # Sample variance
                prior_shape = 2
                prior_scale = prior.phis[k,1]/prior_shape
                post_shape = prior_shape + Nk/2
                post_scale = 1/(1/prior_scale + np.sum((data[idx]-mu)**2)/2)
                tau = np.random.gamma(post_shape, scale=post_scale)
                # Update
                phis_new[k,0] = mu
                phis_new[k,1] = tau

        if np.isnan(phis_new).any():
            raise ValueError('Nan encountered in sample_phis().')

        return phis_new

    def sample_ulist(self):

        alpha = 1
        beta = 1
        epsilon = 9
        data = self.data
        uold = self.old.ulist
        Nbins = self.Nbins

        prob_old = self.loglikelihood(sum_it=False) \
                   + stats.beta.logpdf(uold, alpha, beta)

        unew = np.random.beta(epsilon*uold+1, epsilon*(1-uold)+1)
        prob_new = self.loglikelihood(ulist=unew, sum_it=False) \
                   + stats.beta.logpdf(unew, alpha, beta)

        acc_prob = prob_new - prob_old \
                 + stats.beta.logpdf(uold, epsilon*unew+1, epsilon*(1-unew)+1) \
                 - stats.beta.logpdf(unew, epsilon*uold+1, epsilon*(1-uold)+1)

        idx = acc_prob > np.log(np.random.rand(1,Nbins))

        unew[idx] = uold[idx]

        if np.isnan(unew).any():
            raise ValueError('Nan encountered in sample_ulist().')

        return unew

    # <><><>><><><<><> GIBBS SAMPLER <><><><>><>><<><><><><>><><<><><>><><<><><>><<><><>
    # <><><>><><><<><> GIBBS SAMPLER <><><><>><>><<><><><><>><><<><><>><><<><><>><<><><>
    # <><><>><><><<><> GIBBS SAMPLER <><><><>><>><<><><><><>><><<><><>><><<><><>><<><><>
    # <><><>><><><<><> GIBBS SAMPLER <><><><>><>><<><><><><>><><<><><>><><<><><>><<><><>
    def run_Gibbs(self, Ntrials=1000, sample_beta=True, print_status=True, plot_status=False, save=False):

        prefix = self.prefix
        prior = self.prior
        old = prior.copy()
        old.init(self.data, setup_ell=1, sample_beta=sample_beta)
        MAP = old.copy() # Note that this MAP is not a real max a posteriori, it is a max likelihood
        MAP_prob = -np.inf

        self.Ntrials = Ntrials
        self.old = old
        self.MAP = MAP

        if save:
            history = hmm.history(old, prefix)

        if plot_status:
            plt.show()

        for trial in range(Ntrials):
            t0 = time.time()
            if print_status:
                print('Trial {} {}'.format(trial, prefix))
                print('{} states'.format(len(np.unique(old.states))))
            if plot_status:
                self.show_old(trial)

            if print_status:
                print('states')
            old.states = self.sample_states()

            if print_status:
                print('drift')
            old.drift = self.sample_drift_np()
#            self.sample_drift_cupy()
#            sample_drift_nb(self.data + self.old.ulist,self.old.phis,self.old.states,self.old.Nstates,
#                                        self.old.Nbins,self.old.Ngrid,self.old.Kstar,self.old.KgridInv)

            if print_status:
                print('transitions')
            if self.old.iHMM:
                old.beta, old.pi0, old.pis = self.sample_pis_iHMM(old)
            else:
                old.pi0, old.pis = self.sample_pis(old, prior)

            if print_status:
                print('emissions')
            old.phis = self.sample_phis(old, prior)

            if print_status:
                print('ulist')
            self.old.ulist = self.sample_ulist()

            # Compare to MAP
            MAP_prob_new = self.loglikelihood()
            if MAP_prob_new > MAP_prob:
                MAP = old.copy()
                MAP_prob = MAP_prob_new
                self.MAP = MAP
                if print_status:
                    print('New MAP!!!')

            # fill history
            if save:
                if print_status: print('checkpoint!')
                history.checkpoint(old)
            print('Iter Time: '+str(time.time()-t0))

        print('done')
        plt.close('all')

    def show_old(self, trial=-1):

        prefix = self.prefix
        old = self.old

        data = old.data
        Nbins = old.Nbins
        Nstates = old.Nstates
        lock_in = old.lock_in

        plt.clf()
        plt.plot(data[0,:], color='green', label='data')

        trace   = np.zeros((1,Nbins))
        for k in range(Nstates):
            IDs = old.states==k
            trace[IDs] = old.phis[k,0]
            if np.any(IDs):
                if lock_in[0,k] == 0:
                    color = 'blue'
                else:
                    color = 'grey'
                plt.plot(old.phis[k,0] + old.drift[0,:], color=color)
        trace += old.drift

        plt.plot(trace[0,:], color='red', label='trace')
        plt.title(prefix+'\nTrial {}\n {} states'.format(trial, len(np.unique(old.states))))
        plt.legend(loc='upper right')
        plt.draw()
        plt.pause(1)

    def show_states(self, state_colors=None):

        prefix = self.prefix
        old = self.old

        Nbins = old.Nbins
        Nstates = old.Nstates
        states = old.states

        if state_colors is None:
            idx = old.lock_in == 0
            state_colors = idx + 0.0
            state_colors[idx] = np.cumsum(state_colors[idx])

        plt.clf()
        plt.xlim([0, Nbins])

        colors = ["k",  "red", "blue", "green", "cyan", "orange", "yellow", "indigo", "violet", "magenta", "lawngreen", "turquoise", "deeppink", "gold", "coral", "peru", "red", "blue", "orange", "yellow", "green", "cyan", "indigo", "violet", "magenta", "lawngreen", "turquoise", "deeppink", "gold", "coral", "peru", ]

        for k in range(Nstates):
            if state_colors[0,k] > 0:
                plt.vlines(np.where(states==k)[1], 0, 1, color=colors[int(state_colors[0,k])])
                # obviously color selection should be better, but this works for now

        plt.title(prefix)
        plt.draw()
        plt.pause(1)
























