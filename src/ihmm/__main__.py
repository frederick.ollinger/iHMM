"""Command line interface for iHMM.

See https://docs.python.org/3/using/cmdline.html#cmdoption-m for why module is
named __main__.py.
"""


import typer


app = typer.Typer(
    help="Run iHMM"
)


if __name__ == "__main__":
    app()
