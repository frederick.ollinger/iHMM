FROM registry.gitlab.com/roswellbiotech/signal-processing/pipeline:0.0.5
ARG USER=developer

WORKDIR /home/$USER

# Create VSCode directories or the installation will fail
# Actual extensions are installed in .devcontainer/devcontainer.json
RUN mkdir -p /home/$USER/.vscode-server/extensions && mkdir -p /home/$USER/.vscode-server/extensionsCache

RUN python3 -m pip install --upgrade pip \
&& python3 -m pip install --user poetry

# Copy repository files and change their owner to developer.
COPY --chown=$USER:root .  .

# Export Python packages from pyproject.toml to requirements.txt.
# Avoids need for Poetry virtual environment inside container.
# Args:
#   --user: Install only for current user.
#   --dev: Include development dependencies.
#   --without-hashes: Exclude package hashes from exported file.
RUN poetry export --dev --without-hashes -f requirements.txt -o requirements.txt && \
python3 -m pip install --user -r requirements.txt 

CMD ["/bin/bash"]
